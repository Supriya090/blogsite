import CreatePost from "../components/CreatePost";
import DisplayAll from "../components/DisplayAll";
import Header from "../components/Header";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import ModifyPost from "../components/ModifyPost";
import useLocalStorage from "../customHooks/useLocalStorage";
import FullBlog from "../components/FullBlog";
function AppRouter() {
  const [allPosts, setAllPosts] = useLocalStorage("allPosts", "");

  return (
    <Router>
      <div>
        <Header />
        <div>
          <Switch>
            <Route
              render={(props) => (
                <DisplayAll
                  {...props}
                  allPosts={allPosts}
                  setAllPosts={setAllPosts}
                />
              )}
              path='/'
              exact={true}
            />
            <Route
              render={(props) => (
                <CreatePost
                  {...props}
                  allPosts={allPosts}
                  setAllPosts={setAllPosts}
                />
              )}
              path='/create'
            />
            <Route
              render={(props) => (
                <ModifyPost
                  {...props}
                  allPosts={allPosts}
                  setAllPosts={setAllPosts}
                />
              )}
              path='/edit/:id'
            />
            <Route
              render={(props) => (
                <FullBlog
                  {...props}
                  allPosts={allPosts}
                  setAllPosts={setAllPosts}
                />
              )}
              path='/blog/:id'
            />
          </Switch>
          <Route component={() => <Redirect to='/' />} />
        </div>
      </div>
    </Router>
  );
}

export default AppRouter;
