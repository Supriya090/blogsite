import React from "react";
import { useParams } from "react-router";
import PostForm from "./PostForm";

function ModifyPost({ history, allPosts, setAllPosts }) {
  const { id } = useParams();
  const editPost = allPosts.find((post) => post.id === id);

  const handleOnSubmit = (post) => {
    const remainingPosts = allPosts.filter((post) => post.id !== id);
    setAllPosts([post, ...remainingPosts]);
    history.push("/");
  };

  return (
    <div>
      <PostForm post={editPost} handleOnSubmit={handleOnSubmit} />
    </div>
  );
}

export default ModifyPost;
