import React from "react";
import { NavLink } from "react-router-dom";

function Header() {
  return (
    <header>
      <nav className='navbar'>
        <div className='container'>
          <NavLink
            to='/'
            className='navbar-brand test'
            style={{ fontSize: "30px" }}>
            <span>Supriya's Blog</span>
          </NavLink>
          <div
            className='navbar-nav'
            style={{ display: "flex", flexDirection: "row" }}>
            <NavLink to='/' className='nav-link'>
              All Posts
            </NavLink>
            <NavLink to='/create' className='nav-link'>
              Add Posts
            </NavLink>
          </div>
        </div>
      </nav>
    </header>
  );
}

export default Header;
