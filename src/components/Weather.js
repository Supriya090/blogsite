import React from "react";
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCloud,
  faBolt,
  faCloudRain,
  faCloudShowersHeavy,
  faSnowflake,
  faSun,
  faSmog,
  faRedo,
} from "@fortawesome/free-solid-svg-icons";

const WeatherCard = ({ weatherData }) => {
  const refresh = () => {
    window.location.reload();
  };

  let weatherIcon = null;

  if (weatherData.weather[0].main === "Thunderstorm") {
    weatherIcon = <FontAwesomeIcon icon={faBolt} />;
  } else if (weatherData.weather[0].main === "Drizzle") {
    weatherIcon = <FontAwesomeIcon icon={faCloudRain} />;
  } else if (weatherData.weather[0].main === "Rain") {
    weatherIcon = <FontAwesomeIcon icon={faCloudShowersHeavy} />;
  } else if (weatherData.weather[0].main === "Snow") {
    weatherIcon = <FontAwesomeIcon icon={faSnowflake} />;
  } else if (weatherData.weather[0].main === "Clear") {
    weatherIcon = <FontAwesomeIcon icon={faSun} />;
  } else if (weatherData.weather[0].main === "Clouds") {
    weatherIcon = <FontAwesomeIcon icon={faCloud} />;
  } else {
    weatherIcon = <FontAwesomeIcon icon={faSmog} />;
  }

  return (
    <div className='weather-div'>
      <div className='location'>
        <div className='refresh-cover'>
          <FontAwesomeIcon
            icon={faRedo}
            onClick={refresh}
            className='refresh-icon'
          />
        </div>
        {weatherData.name}, {weatherData.sys.country}
        <div className='date'>
          {moment().format("LL")}, {moment().format("LT")}
        </div>
      </div>
      <div className='weather-box'>
        <span className='temp'>
          <span>{weatherIcon}</span>
          {Math.round(weatherData.main.temp)}°C
        </span>
      </div>
    </div>
  );
};

export default WeatherCard;
