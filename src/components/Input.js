import React from "react";
import "../App.css";

function Input({ value, label, id, ...inputProps }) {
  return (
    <div className='form-inputs'>
      <label htmlFor={id} className='form-label'>
        {label}
      </label>
      <input id={id} value={value} {...inputProps} />
    </div>
  );
}

export default Input;
