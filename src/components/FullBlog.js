import React from "react";
import { useParams } from "react-router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendar, faHeart } from "@fortawesome/free-solid-svg-icons";
import Post from "./Post";

function FullBlog({ allPosts, setAllPosts }) {
  const { id } = useParams();
  const eachBlog = allPosts.find((post) => post.id === id);

  const deletePost = (id) => {
    const remainingPosts = allPosts.filter((eachPost) => {
      return eachPost.id !== id;
    });
    setAllPosts(remainingPosts);
  };

  return (
    <div className='container each-blog'>
      <div className='blog-item'>
        <div className='blog-img'>
          <img src={eachBlog.image} alt={eachBlog.title} />
          <span>
            <FontAwesomeIcon icon={faHeart} />
          </span>
        </div>
        <div className='blog-text'>
          <div>
            <span className='author-name'>
              <FontAwesomeIcon icon={faCalendar} />
              <span>{eachBlog.date}</span>
            </span>
          </div>
          <h2>{eachBlog.title}</h2>
          <p className='main-content'>{eachBlog.content}</p>
        </div>
      </div>
      <div>
        <hr style={{ height: "1px", background: "black" }} />
        <div className='more-blogs'>
          {allPosts.length > 1 ? (
            <h3>More for you...</h3>
          ) : (
            <h3>No new posts</h3>
          )}

          <div className='blog-content'>
            {allPosts.map((eachPost) => {
              return (
                <>
                  {eachPost.id !== id ? (
                    <Post
                      key={eachPost.id}
                      {...eachPost}
                      deletePost={deletePost}
                    />
                  ) : (
                    ""
                  )}
                </>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}

export default FullBlog;
