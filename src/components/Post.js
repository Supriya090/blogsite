import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendar, faHeart } from "@fortawesome/free-solid-svg-icons";
import { useHistory } from "react-router";
import "../App.css";

function Post({ id, title, content, like, image, date, deletePost }) {
  const onLiked = () => {
    like = !like;
  };

  const history = useHistory();
  return (
    <div className='blog-item'>
      <div className='blog-img'>
        <a
          className='read-more'
          onClick={() => {
            history.push(`/blog/${id}`);
          }}>
          <img src={image} alt={title} />
        </a>
        <span>
          <FontAwesomeIcon
            icon={faHeart}
            onClick={() => {
              onLiked();
              console.log(like);
            }}
          />
        </span>
      </div>
      <div className='blog-text'>
        <div>
          <span className='author-name'>
            <FontAwesomeIcon icon={faCalendar} />
            <span
              style={{
                color: like ? "pink" : "",
              }}>
              {date}
              {console.log(like)}
            </span>
          </span>
        </div>
        <a
          className='read-more'
          onClick={() => {
            history.push(`/blog/${id}`);
          }}>
          <h2>{title}</h2>
        </a>
        <div className='content-readmore-div'>
          <div className='content-div'>
            <p className='main-content'>{content}</p>
          </div>
          <a
            onClick={() => {
              history.push(`/blog/${id}`);
            }}
            className='read-more'>
            Read Full
          </a>
        </div>
        <button onClick={() => history.push(`/edit/${id}`)} className='buttons'>
          Edit
        </button>{" "}
        <button onClick={() => deletePost(id)} className='buttons'>
          Delete
        </button>
      </div>
    </div>
  );
}

export default Post;
