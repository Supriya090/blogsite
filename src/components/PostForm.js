import { Button } from "react-bootstrap";
import React, { useState } from "react";
import { v4 as uuidv4 } from "uuid";
import Input from "./Input";

const PostForm = (props) => {
  const [post, setPost] = useState(() => {
    return {
      title: props.post ? props.post.title : "",
      content: props.post ? props.post.content : "",
      like: props.post ? props.post.like : false,
      image: props.post
        ? props.post.image
        : "https://thumbs.dreamstime.com/b/generic-landscape-hill-field-white-flowers-blue-sky-big-white-clouds-51103567.jpg",
      date: props.post ? props.post.date : "",
    };
  });

  const [errorMsg, setErrorMsg] = useState("");
  const { title, content, image, like } = post;

  const handleOnSubmit = (e) => {
    e.preventDefault();
    const values = [title, content];
    let errorMsg = "";

    const allFieldsFilled = values.every((field) => {
      const value = `${field}`;
      return value !== "" && value !== "0";
    });

    if (allFieldsFilled) {
      const date = new Date();
      const post = {
        id: uuidv4(),
        title,
        content,
        image,
        like,
        date: date.toLocaleDateString(),
      };
      console.log(post.date);
      props.handleOnSubmit(post);
    } else {
      errorMsg = "Please fill out all the fields.";
    }
    setErrorMsg(errorMsg);
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setPost((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  return (
    <div className='form-background'>
      <div className='form-container'>
        <div className='form-content'>
          <form onSubmit={handleOnSubmit} className='form'>
            <Input
              id='title'
              type='text'
              name='title'
              label='Blog Title'
              value={title}
              className='form-input'
              placeholder='Enter Blog Title'
              onChange={handleInputChange}
            />
            <Input
              id='image'
              type='text'
              name='image'
              label='Image URL'
              value={image}
              className='form-input'
              placeholder='Enter the URL of image'
              onChange={handleInputChange}
            />
            <div className='form-inputs'>
              <label htmlFor='content' className='form-label'>
                Blog Content
              </label>
              <textarea
                id='content'
                name='content'
                rows='7'
                cols='50'
                placeholder='Type Blog Content Here'
                value={content}
                onChange={handleInputChange}
                className='form-input blog-textarea'></textarea>
            </div>
            {errorMsg && <p className='errorMsg'>{errorMsg}</p>}
            <Button
              variant='secondary'
              type='submit'
              className='buttons submit-btn'>
              Submit
            </Button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default PostForm;
