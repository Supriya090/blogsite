import React from "react";
import PostForm from "./PostForm";

function CreatePost({ history, allPosts, setAllPosts }) {
  const handleOnSubmit = (post) => {
    setAllPosts([post, ...allPosts]);
    history.push("/");
  };

  return (
    <React.Fragment>
      <PostForm handleOnSubmit={handleOnSubmit} />
    </React.Fragment>
  );
}

export default CreatePost;
