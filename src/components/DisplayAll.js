import React from "react";
import Post from "./Post";
import "../App.css";
import { Link } from "react-router-dom";
import DisplayWeather from "./DisplayWeather";

function DisplayAll({ allPosts, setAllPosts }) {
  const deletePost = (id) => {
    const remainingPosts = allPosts.filter((eachPost) => {
      return eachPost.id !== id;
    });
    setAllPosts(remainingPosts);
  };

  return (
    <React.Fragment>
      <div className='banner'>
        <div className='container'>
          <h1 className='banner-title'>
            <span>Supriya's</span> Blog
          </h1>
          <p>Welcome to the world of my thoughts!</p>
          <DisplayWeather />
        </div>
      </div>
      {!allPosts.length ? (
        <section className='blog' id='blog'>
          <div className='container no-content'>
            <h3>No posts available!</h3>
            <h3>Would you like to create one?</h3>
            <Link to='/create' className='buttons'>
              Create New
            </Link>
          </div>
        </section>
      ) : (
        <>
          <section className='blog' id='blog'>
            <div className='container'>
              <div className='blog-content'>
                {allPosts.map((eachPost) => {
                  return (
                    <>
                      {console.log(eachPost)}
                      <Post
                        key={eachPost.id}
                        {...eachPost}
                        deletePost={deletePost}
                      />
                    </>
                  );
                })}
              </div>
            </div>
          </section>

          <div style={{ textAlign: "center", marginBottom: "30px" }}>
            <Link to='/create' className='buttons'>
              Create New
            </Link>
          </div>
        </>
      )}
    </React.Fragment>
  );
}

export default DisplayAll;
